# 🐋 adrift/docker/tor-node

> Lightweight TOR relay image (13.37 MB), based on [LinuxServer.io Alpine Linux](https://linuxserver.io). Configurations can be passed as environment variables.

## Usage

### Bridge mode

```bash
docker run \
  -d \
  --name tor-node-bridge \
  -e RELAY_TYPE=bridge \
  -e TOR_ORPort=9001 \
  -e TOR_DirPort=9030 \
  -e TOR_Nickname=ididnteditheconfig \
  -e TOR_RelayBandwidthRate="40000 KBytes" \
  -e TOR_RelayBandwidthBurst="100000 KBytes" \
  -e PUID=$(id -u) \
  -e PGID=$(id -g) \
  -v $(pwd)/tor/data:/data:Z \
  -p 9001:9001 \
  -p 9030:9030 \
  --restart always \
  registry.gitlab.com/adrift/docker/tor-node
```

### Relay mode

```bash
docker run \
  -d \
  --name tor-node-relay \
  -e RELAY_TYPE=relay \
  -e TOR_ORPort=9001 \
  -e TOR_DirPort=9030 \
  -e TOR_Nickname=ididnteditheconfig \
  -e TOR_RelayBandwidthRate="40000 KBytes" \
  -e TOR_RelayBandwidthBurst="100000 KBytes" \
  -e PUID=$(id -u) \
  -e PGID=$(id -g) \
  -v $(pwd)/tor/data:/data:Z \
  -p 9001:9001 \
  -p 9030:9030 \
  --restart always \
  registry.gitlab.com/adrift/docker/tor-node
```

### Exit node mode

**It is highly recommended against setting up this type of configuration in a home network.**

[Tips for Running an Exit Node.](https://blog.torproject.org/tips-running-exit-node)

```bash
docker run \
  -d \
  --name tor-node-exit \
  -e RELAY_TYPE=exit \
  -e TOR_ORPort=9001 \
  -e TOR_DirPort=9030 \
  -e TOR_Nickname=ididnteditheconfig \
  -e TOR_RelayBandwidthRate="40000 KBytes" \
  -e TOR_RelayBandwidthBurst="100000 KBytes" \
  -e PUID=$(id -u) \
  -e PGID=$(id -g) \
  -v $(pwd)/tor/data:/data:Z \
  -p 9001:9001 \
  -p 9030:9030 \
  --restart always \
  registry.gitlab.com/adrift/docker/tor-node
```

## Configuration

The configuration is stored in a `/etc/tor/torrc` file.
Everyline can be changed using environment variables as described below :

`TOR_<configuration>=<value>` will uncomment the first line starting with `<configuration>` and set its value to `<value>`.

*Example : setting `TOR_ORPort` to `9002` will change the line `#ORPort 9001` to `ORPort 9002`.*

## License

MIT

Thank you to the upstream fork-origin repo! https://github.com/Ilshidur/tor-relay-docker/issues/new"
